Introduction
=============
Hi there! Thanks for reviewing my solution to this challenge. I have done my best to approach this project in a functional, practical way. I believe in writing readable code and have tried to predict future business cases so refactoring is at a minimum.

Getting Started
---------------
The solution is written in functional javascript, using Ramda as the utility library. The main file is `index.js` in the root of the project. To achieve separation of concerns, the project is split into several files in the `lib` directory. Much of the functionality is located there.

To run the project, you need to have `node v6.2.2` or greater installed. Please see https://nodejs.org/ for installation instructions.

1. Open the terminal.
2. Navigate to the project root `cd PROJECT_PATH`.
3. Install npm packages `npm install`.
4. Run the project with an input file. There are multiple ways to do this.
    * Pipe `cat sample-input.txt | node index.js`.
    * Redirect `node index.js < sample-input.txt`.
    * Filepath `node index.js sample-input.txt`.
5. Enjoy!
6. If you would like to run tests, use `npm test`.

Looking forward to hearing your thoughts. Cheers!

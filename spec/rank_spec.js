const R = require('ramda');
const chai = require('chai');
const util = require('./util.js');
const input = require('../lib/input.js');
const rank = require('../lib/rank.js');

describe('rank', () => {
  /** hooks */
  let rawInput;
  let teams;
  before((done) => {
    util.getSampleData()
      .then((data) => {
        rawInput = data;
        teams = input.processInput(data);
        return done();
      })
      .catch(done)
  });

  /** tests */
  it('does place teams in array', () => {
    const placed = rank.placeTeams(teams);
    chai.expect(placed).to.an('array');
  });

  it('does place all teams', () => {
    const places = R.chain(
      R.compose(R.values, R.pick(['place'])),
      rank.placeTeams(teams)
    );
    chai.expect(R.remove(0, 0, places)).to.deep.equal(places);
  });

  it('does rank winning teams correctly', () => {
    const team1 = { score: 2, leaguePoints: 0, gamesLost: 0, gamesWon: 0 };
    const team2 = { score: 0, leaguePoints: 0, gamesLost: 0, gamesWon: 0 };
    const [ winningTeam, losingTeam ] = rank.rankTeams(team1, team2)

    chai.expect(winningTeam.leaguePoints).to.equal(3);
    chai.expect(winningTeam.gamesLost).to.equal(0);
    chai.expect(winningTeam.gamesWon).to.equal(1);

    chai.expect(losingTeam.leaguePoints).to.equal(0);
    chai.expect(losingTeam.gamesLost).to.equal(1);
    chai.expect(losingTeam.gamesWon).to.equal(0);
  });

  it('does rank tied teams correctly', () => {
    const team1 = { score: 2, leaguePoints: 0 };
    const team2 = { score: 2, leaguePoints: 0 };
    const [ winningTeam, losingTeam ] = rank.rankTeams(team1, team2);

    chai.expect(winningTeam.leaguePoints).to.equal(losingTeam.leaguePoints);
  });
});

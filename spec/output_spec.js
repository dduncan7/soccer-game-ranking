const R = require('ramda');
const chai = require('chai');
const util = require('./util.js');
const input = require('../lib/input.js');
const output = require('../lib/output.js');

describe('output', () => {
  /** hooks */
  let teams;
  before((done) => {
    util.getSampleData()
      .then((data) => {
        teams = input.processInput(data);
        return done();
      })
      .catch(done)
  });

  /** tests */
  it('does plural words correctly', () => {
    chai.expect(output.determinePlural(0, 'pt')).to.equal('pts');
    chai.expect(output.determinePlural(1, 'pt')).to.equal('pt');
    chai.expect(output.determinePlural(2, 'pt')).to.equal('pts');
    chai.expect(output.determinePlural(10, 'pt')).to.equal('pts');
  });

  it('does populate row', () => {
    const team = teams[0];
    const hasUndefined = R.match(/undefined/, output.row(team));
    chai.expect(hasUndefined).to.be.empty;
  });

  it('does create full table with newlines', () => {
    const table = output.table(teams);
    chai.expect(R.split('\n', table).length).to.equal(teams.length);
  });
});

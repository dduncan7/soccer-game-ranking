const fs = require('fs');
const path = require('path');

/**
 * Get the sample data for tests
 * @return {Promise}
 */
const getSampleData = () => {
  return new Promise((resolve, reject) => {
    const filePath = path.resolve(__dirname, '..', 'sample-input.txt');
    fs.readFile(filePath, 'utf8', (err, data) => {
      if (err) return reject(err);
      return resolve(data);
    })
  });
};

module.exports = { getSampleData };

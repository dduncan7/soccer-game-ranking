const R = require('ramda');
const chai = require('chai');
const util = require('./util.js');
const input = require('../lib/input.js');

describe('input', () => {
  /** hooks */
  let teams;
  before((done) => {
    util.getSampleData()
      .then((data) => {
        teams = input.processInput(data);
        return done();
      })
      .catch(done)
  });

  /** tests */
  it('does format input into an array', () => {
    chai.expect(teams).to.be.an('array');
  });

  it('is sorted from largest to smallest', () => {
    const sortedPoints = R.compose(
      R.pluck('leaguePoints'),
      R.reverse,
      R.sortBy(R.prop('leaguePoints'))
    )(teams);
    const processedPoints = R.pluck('leaguePoints', teams);

    chai.expect(sortedPoints).to.deep.equal(processedPoints);
  });

  it('does not have duplicate teams', () => {
    const names = R.values(R.pick(['name'], teams));
    chai.expect(R.uniq(names)).to.deep.equal(names);
  });
});

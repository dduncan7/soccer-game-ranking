/** dependencies */
const R = require('ramda');

/**
 * Determines the placing of the team
 * @param {Array}
 * @return {Array}
 */
const placeTeams = R.reduce((acc, x) => {
  const previousPlacedTeam = R.last(acc) || { place: 0 };
  const samePlacedTeam = R.find(R.propEq('leaguePoints', x.leaguePoints))(acc);
  x.place = samePlacedTeam ? samePlacedTeam.place : previousPlacedTeam.place + 1;
  acc.push(x);
  return acc;
}, []);

/**
 * Compare two teams and rank them based off the results
 * @param {Object} team1
 * @param {Object} team2
 * @return {Array}
 */
const rankTeams = (team1, team2) => {
  const _team1 = R.merge({}, team1);
  const _team2 = R.merge({}, team2);

  const firstLost = _team1.score < _team2.score;
  if (firstLost) {
    _team1.leaguePoints += 0;
    _team1.gamesLost += 1;
    _team2.leaguePoints += 3;
    _team2.gamesWon += 1;
  }

  const firstWon = _team1.score > _team2.score;
  if (firstWon) {
    _team1.leaguePoints += 3;
    _team1.gamesWon += 1;
    _team2.leaguePoints += 0;
    _team2.gamesLost += 1;
  }

  const bothTied = _team1.score === _team2.score;
  if (bothTied) {
    _team1.leaguePoints += 1;
    _team1.gamesTied += 1;
    _team2.leaguePoints += 1;
    _team2.gamesTied += 1;
  }

  return [_team1, _team2];
};

module.exports = { placeTeams, rankTeams };

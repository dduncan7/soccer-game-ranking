/** dependencies */
const R = require('ramda');
const rank = require('./rank.js');

/**
 * Create an object to identity the team name and score
 * @param {Array} team
 * @return {Object}
 */
const processTeam = (team) => {
  /** NOTE: the game score is always last, so split the array based on that fact. */
  const getName = R.compose(R.join(' '), R.dropLast(1));
  const getScore = R.compose(parseInt, R.last);

  const teamObject = {
    name: getName(team),
    place: 0,
    score: getScore(team) || 0,
    leaguePoints: 0,
    gamesWon: 0,
    gamesLost: 0,
    gamesTied: 0,
  };
  return teamObject;
};

/**
 * Translate the teams in a game into their team objects
 * @param {String}
 * @return {Array}
 */
const processTeams = R.compose(
  processTeam,
  R.split(' '),
  R.trim
);

/**
 * Separates the games and process the teams
 * @param {String}
 * @return {Array}
 */
const processGame = R.compose(
  R.map(processTeams),
  R.split(',')
);

/**
 * Merges duplicate team records into a single record
 * @param {Object} acc
 * @param {Object} team
 * @return {Object}
 */
const dedupeTeams = R.curry((acc, team) => {
  const currentTeam = acc[team.name] || {};

  const combinedTeam = R.mergeWith((val1, val2) => {
    if (R.is(Number, val1) && R.is(Number, val2)) {
      return val1 + val2;
    } else {
      return val1;
    }
  }, team, currentTeam);

  acc[team.name] = combinedTeam;
  return acc;
});

/**
 * Rank the teams in a game
 * @param {Array} acc
 * @param {Array} teams
 * @return {Array}
 */
const rankGames = (acc, teams) => {
  const rankedTeams = rank.rankTeams(teams[0], teams[1]);
  acc.push(rankedTeams);
  return acc;
};

/**
 * Transform input string into structured teams
 * @param {String}
 * @return {Array}
 */
const processInput = R.compose(
  rank.placeTeams,
  R.compose(R.reverse, R.sortBy(R.prop('leaguePoints'))),
  // reduce into an object to make deduping/merging easier
  R.compose(R.values, R.reduce(dedupeTeams, {})),
  R.flatten,
  R.reduce(rankGames, []),
  R.map(processGame),
  R.filter(R.identity),
  R.split('\n')
);

module.exports = {
  processTeam,
  processTeams,
  processGame,
  dedupeTeams,
  processInput,
};

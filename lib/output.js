/** dependencies */
const R = require('ramda');

/**
 * Pluralrize string when number is greater then one
 * @param {Number} x
 * @param {String} str
 * @return {String}
 */
const determinePlural = R.curry((x, str) => x === 1 ? str : str + 's');

/**
 * Outputs team as table row
 * @param {Object} team
 * @return {String}
 */
const row = (team) => {
  const suffix = determinePlural(team.leaguePoints, 'pt');
  return `${team.place}. ${team.name}, ${team.leaguePoints} ${suffix}`;
};

/**
 * Creates a table of team rankings
 * @param {Array}
 * @return {String}
 */
const table = R.compose(R.join('\n'), R.map(row));

module.exports = { determinePlural, row, table };

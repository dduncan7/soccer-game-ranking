const fs = require('fs');
const path = require('path');
const input = require('./lib/input.js');
const output = require('./lib/output.js');

const fileName = process.argv[2];
const readStream = fileName
  ? fs.createReadStream(path.resolve(__dirname, fileName))
  : process.stdin;

/**
 * Read either the file or stdin stream.
 */
let data = '';

readStream.setEncoding('utf8');
readStream.resume();

readStream.on('data', (chunk) => {
  data += chunk;
});

readStream.on('error', (err) => {
  console.error(err);
  throw new Error('Input data was invalid.');
});

readStream.on('end', () => {
  /** init with our input data */
  console.log(output.table(input.processInput(data)));
});
